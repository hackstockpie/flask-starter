from flask import render_template, session, redirect, url_for
from . import admin
from app import db
from models import *


@admin.route('/', methods=['GET'])
def index():
    return 'Welcome to Flask-Starter'
